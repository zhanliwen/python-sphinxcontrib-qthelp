%global pypi_name sphinxcontrib-qthelp

%bcond_without check

Name:           python-%{pypi_name}
Version:        1.0.3
Release:        2
Summary:        Sphinx extension for QtHelp documents
License:        BSD
URL:            http://sphinx-doc.org/
Source0:        https://files.pythonhosted.org/packages/source/s/sphinxcontrib-qthelpx/sphinxcontrib-qthelp-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  gettext
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%if %{with check}
BuildRequires:  python3-pytest
BuildRequires:  python3-sphinx
%endif

%description
sphinxcontrib-qthelp is a sphinx extension which outputs QtHelp document.


%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name}
sphinxcontrib-qthelp is a sphinx extension which outputs QtHelp document.


%prep
%autosetup -n %{pypi_name}-%{version}
find -name '*.mo' -delete


%build
for po in $(find -name '*.po'); do
  msgfmt --output-file=${po%.po}.mo ${po}
done
%py3_build


%install
%py3_install

# Move language files to /usr/share
pushd %{buildroot}%{python3_sitelib}
for lang in `find sphinxcontrib/qthelp/locales -maxdepth 1 -mindepth 1 -type d -not -path '*/\.*' -printf "%f "`;
do
  test $lang == __pycache__ && continue
  install -d %{buildroot}%{_datadir}/locale/$lang/LC_MESSAGES
  mv sphinxcontrib/qthelp/locales/$lang/LC_MESSAGES/*.mo %{buildroot}%{_datadir}/locale/$lang/LC_MESSAGES/
done
rm -rf sphinxcontrib/qthelp/locales
ln -s %{_datadir}/locale sphinxcontrib/qthelp/locales
popd


%find_lang sphinxcontrib.qthelp


%if %{with check}
%check
%{__python3} tests/conftest.py
%endif


%files -n python3-%{pypi_name} -f sphinxcontrib.qthelp.lang
%license LICENSE
%doc README.rst
%{python3_sitelib}/sphinxcontrib/
%{python3_sitelib}/sphinxcontrib_qthelp-%{version}-py%{python3_version}-*.pth
%{python3_sitelib}/sphinxcontrib_qthelp-%{version}-py%{python3_version}.egg-info/


%changelog
* Sat Aug 1 2020 shixuantong <shixuantong@huawei.com> - 1.03-2
- Modify Source0

* Fri Jul 31 2020 tianwei <tianwei12@huawei.com> - 1.0.3-1
- Package Init
